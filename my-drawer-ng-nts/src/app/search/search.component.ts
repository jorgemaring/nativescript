import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { NoticiasService } from "../domain/noticias.service";
import { Color } from "tns-core-modules/color";
import { View } from "tns-core-modules/ui/core/view";
import * as Toast from "nativescript-toasts";
import * as appSettings from "tns-core-modules/application-settings";
import * as SocialShare from "nativescript-social-share";
import { Store } from "@ngrx/store";
import { AppState } from "../app.module";
import { Noticia, NuevaNoticiaAction } from "../domain/noticias-state.model";


@Component({
    selector: "Search",
    templateUrl: "./search.component.html",
    providers: [NoticiasService]
})
export class SearchComponent implements OnInit {
    resultados: Array<string> = [];
    @ViewChild("layout", {static: false}) layout: ElementRef;

    constructor(
        private noticias: NoticiasService,
        private store: Store<AppState>) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
        this.store.select((state) => state.noticias.sugerida)
        .subscribe((data) => {
            const f = data;
            if (f != null) {
                Toast.show({text: "Sugerimos leer: " + f.titulo, duration: Toast.DURATION.SHORT});
            }
        });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(x) {
        console.log(x);
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(x.view.bindingContext)));
    }
    
    onLongPress(x) {
        console.log(x);
        SocialShare.shareText(x, " este es el asunto que se compartio.");
    }

    onPull(e) {
        console.log(e);
        const pullRefresh = e.object;
        setTimeout(() => {
            this.resultados.push("xxxxxxx");
            pullRefresh.refreshing = false;
        }, 2000);
    }

    searchNow(s) {
        appSettings.setBoolean("busco", true);
        const estaLogueado = appSettings.getBoolean("estaLogueado", false);
        console.log(estaLogueado);
        this.noticias.buscar(s).then((r: any) => {
        console.dir("buscarAhora" + s);
        this.noticias.buscar(s).then((r: any) => {
            console.log("resultados buscarAhora: " + JSON.stringify(r));
            this.resultados = r;
        }, (e) => {
            console.log("error buscarAhora " + e);
            Toast.show({text: "Error en la búsqueda", duration: Toast.DURATION.SHORT});
        });
        });
        const layout = <View>this.layout.nativeElement;
        layout.animate({
            backgroundColor: new Color("blue"),
            duration: 300,
            delay: 150
        }).then(() => layout.animate({
            backgroundColor: new Color("white"),
            duration: 300,
            delay: 150
        }));
    }
}
