import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { registerElement } from "nativescript-angular/element-registry";
registerElement("MapView", () => require("nativescript-google-maps-sdk").MapView);
import * as app from "tns-core-modules/application";
import { device, screen } from "tns-core-modules/platform";
import {
    connectionType,
    getConnectionType,
    startMonitoring,
    stopMonitoring
} from "tns-core-modules/connectivity";
@Component({
    selector: "Browse",
    templateUrl: "./browse.component.html"
})
export class BrowseComponent implements OnInit {
    monitoreando: boolean = false; // una variable para saber si estás monitoreando o no.

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }
    onMonitoreoDatos(): void {
        const myConnectionType = getConnectionType();
        switch (myConnectionType) {
            case connectionType.none:
                console.log("Sin Conexion");
                break;
            case connectionType.wifi:
                console.log("WiFi");
                break;
            case connectionType.mobile:
                console.log("Mobile");
                break;
            case connectionType.ethernet:
                console.log("Ethernet"); // es decir, cableada
                break;
            case connectionType.bluetooth:
                console.log("Bluetooth");
                break;
            default:
                break;
        }
        this.monitoreando = !this.monitoreando;
        if (this.monitoreando) {
            startMonitoring((newConnectionType) => {
                switch (newConnectionType) {
                    case connectionType.none:
                        console.log("Cambió a sin conexión.");
                        break;
                    case connectionType.wifi:
                        console.log("Cambió a WiFi.");
                        break;
                    case connectionType.mobile:
                        console.log("Cambió a mobile.");
                        break;
                    case connectionType.ethernet:
                        console.log("Cambió a ethernet.");
                        break;
                    case connectionType.bluetooth:
                        console.log("Cambió a bluetooth.");
                        break;
                    default:
                        break;
                }
            });
        } else {
            stopMonitoring();
        }
    }
    
    platformData() {
        console.log("modelo", device.model);
        console.log("tipo dispositivo", device.deviceType);
        console.log("Sistema operativo", device.os);
        console.log("versión sist operativo", device.osVersion);
        console.log("Versión sdk", device.sdkVersion);
        console.log("lenguaje", device.language);
        console.log("fabricante", device.manufacturer);
        console.log("código único de dispositivo", device.uuid);
        console.log("altura en pixels normalizados", screen.mainScreen.heightDIPs);
        // DIP (Device Independent Pixel), también conocido como densidad de píxeles independientes.
        // Un píxel virtual que aparece aproximadamente del mismo tamaño en una variedad de densidades de pantalla.
        console.log("altura pixels", screen.mainScreen.heightPixels);
        console.log("escala pantalla", screen.mainScreen.scale);
        console.log("ancho pixels normalizados", screen.mainScreen.widthDIPs);
        console.log("ancho pixels", screen.mainScreen.widthPixels);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    // Map events
    onMapReady(event): void {
        console.log("Map Ready");
    }
}
